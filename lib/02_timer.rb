class Timer
  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

  def padder(time)
    if time >= 10
      time.to_s
    else
      "0" + time.to_s
    end
  end

  def time_string
    sec = @seconds % 60
    min = (@seconds / 60) % 60
    hr = @seconds / 60**2

    padder(hr) + ":" + padder(min) + ":" + padder(sec)
  end
end
