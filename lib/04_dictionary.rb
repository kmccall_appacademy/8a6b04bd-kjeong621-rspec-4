class Dictionary
  # TODO: your code goes here!
  attr_accessor :hash

  def initialize
    @hash = Hash.new
  end

  def entries
    @hash
  end

  def keywords
    @hash.keys.sort
  end

  def add(input)
    if input.instance_of? String
      @hash[input] = nil
    elsif input.instance_of? Hash
      input.each {|k,v=nil| @hash[k] = v}
    end
  end

  def include?(str)
    @hash.keys.include?(str)
  end

  def find(str)
    @hash.select do |k,v|
      k.include?(str)
    end
  end

  def printable
    new_hash = keywords.map do |keyword|
      %Q{[#{keyword}] "#{@hash[keyword]}"}
    end
    new_hash.join("\n")
  end
end
