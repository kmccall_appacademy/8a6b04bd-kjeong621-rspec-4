class Temperature
  #  TODO: your code goes here!
  attr_accessor :fahrenheit, :celsius

  def initialize(hash = {})
    @fahrenheit = hash[:f]
    @celsius = hash[:c]
  end

  def in_fahrenheit
    if @fahrenheit == nil
      @celsius * 9.0/5.0 + 32
    else
      @fahrenheit
    end
  end

  def in_celsius
    if @celsius == nil
      (@fahrenheit - 32) * 5.0/9.0
    else
      @celsius
    end
  end

  def self.from_fahrenheit(temp_f)
    self.new(f: temp_f)
  end

  def self.from_celsius(temp_c)
    self.new(c: temp_c)
  end

  def ftoc(temp_f)
    (temp_f - 32) * 5.0/9.0
  end

  def ctof(temp_c)
    temp_c * 9.0/5.0 + 32
  end
end

class Celsius < Temperature
  def initialize(temp)
    self.celsius = temp
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    self.fahrenheit = temp
  end
end
