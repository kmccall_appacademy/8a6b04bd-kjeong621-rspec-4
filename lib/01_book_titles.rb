class Book
  # TODO: your code goes here!
  EXCLUDE = ["the", "a", "and", "an", "in", "of"]

  attr_writer :title

  def initialize
    @title = ""
  end

  def title
    title_words = @title.split(" ").map.with_index do |word, idx|
      if EXCLUDE.include?(word) && idx != 0
        word
      else
        word.capitalize
      end
    end
    title_words.join(" ")
  end
end
